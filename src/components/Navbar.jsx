import React, { useState } from "react";
import { NavLink } from "react-router-dom";


const Navbar = () => {
  const [click, setClick] = useState(false);


  const handleClick = () => setClick(!click);

  return (
    <div className="flex justify-between items-center h-20 max-w-[1240px] mx-auto px-4 text-black border-b border-[#ebd3aa]">
      <ul>
        <li className="p-4 cursor-pointer nav-item">
          <NavLink
            exact
            to="/User"
            activeclassname="active"
            className="nav-links"
            onClick={handleClick}
          >
            user
          </NavLink>
        </li>   
      </ul>
    </div>
  );
};

export default Navbar;
