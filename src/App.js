import React from 'react';
//import UserComments from './basecamp/UserComments';
import User from "./basecamp/User";
import Navbar from "./components/Navbar";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

function App() {
 
  
  return (
    <div>
      <h1>launchpad link</h1>
      <a href="//launchpad.37signals.com/authorization/new?type=web_server&client_id=6e9b23c155c4957837eb5d73b7a2d9c5ff41f3ff&redirect_uri=https%3A%2F%2Fbasecamp-api.vercel.app%2FUser">
        <button>basecamp launchpad</button>
      </a>
      <Router>
        <Navbar />
        <div>
          <Routes>
            <Route path="/User" element={<User></User>} />
          </Routes>
        </div>
      </Router>
      
    </div>
  );
}

export default App;
