import React, { useState, useEffect } from 'react';
import { fetchUserComments } from './BasecampApi';

function UserComments({ userId, accessToken }) {
  const [comments, setComments] = useState([]);

  useEffect(() => {
    fetchUserComments(userId, accessToken)
      .then(comments => setComments(comments))
      .catch(error => console.error(error));
  }, [userId, accessToken]);

  return (
    <div>
      <h2>User Comments</h2>
      {comments.map(comment => (
        <div key={comment.id}>
          <p>{comment.content}</p>
          <p>Posted by {comment.creator.name}</p>
        </div>
      ))}
    </div>
  );
}

export default UserComments;