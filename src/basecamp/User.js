import React, { useState, useEffect } from "react";

const User = () => {
  const [accounts, setAccounts] = useState([]);
  const [accessToken, setAccessToken] = useState(null);
  useEffect(() => {
    const getAccessToken = async (code) => {
      try {
        const response = await fetch(
          "https://launchpad.37signals.com/authorization/token",
          {
            method: "POST",
            headers: {
              "Content-Type": "application/x-www-form-urlencoded",
            },
            body: new URLSearchParams({
              client_id: "6e9b23c155c4957837eb5d73b7a2d9c5ff41f3ff",
              client_secret: "3c146ef7e4234dab88ef128f4a549657945670ab",
              code: code,
              grant_type: "authorization_code",
              redirect_uri: "https://basecamp-api.vercel.app/User",
            }),
          }
        );
        
        const data = await response.json();
        setAccessToken(data.access_token);
        console.log("show error");
        console.log("Access token:", data.access_token);
      } catch (error) {
        console.error(error);
      }
    };

    const queryParams = new URLSearchParams(window.location.search);
    const code = queryParams.get("code");
    if (code) {
      getAccessToken(code);
    }

    if (accessToken) {
      const fetchAccount = async (accessToken) => {
        try {
          const response = await fetch(
            "https://3.basecampapi.com/3743959/api/v1/people/2.json",
            {
              headers: {
                Authorization: `Bearer ${accessToken}`,
              },
            }
          );
          const data = await response.json();
          setAccounts([data]);
        } catch (error) {
          console.error(error);
        }
      };
      fetchAccount(accessToken);
    }
  }, [accessToken]);

  return (
    <>
      <h1>User page</h1>
      <div>
        <h2>User Account</h2>
        {accounts.map((accounts) => (
          <div key={accounts.id}>
            <p>Name: {accounts.first_name}</p>
            <p>Email: {accounts.email_address}</p>
          </div>
        ))}
      </div>
    </>
  );
};

export default User;
