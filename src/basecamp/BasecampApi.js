export async function fetchUserComments(userId, accessToken) {
  const response = await fetch(`https://3.basecampapi.com/24843240/buckets/1/recordings/3/comments.json`, {
    headers: {
      'Authorization': `Bearer ${accessToken}`,
      'User-Agent': 'https://basecamp-api.vercel.app/',
      'Accept': 'application/json'
    }
  });

  if (!response.ok) {
    throw new Error(`Failed to fetch comments: ${response.statusText}`);
  }

  const data = await response.json();

  const comments = data.reduce((acc, bucket) => {
    if (bucket.conversations) {
      bucket.conversations.forEach(conversation => {
        if (conversation.comments) {
          acc = [...acc, ...conversation.comments];
        }
      });
    }

    return acc;
  }, []);

  return comments;
}
